<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaskDefinition extends Model
{
    use CrudTrait;
    use HasFactory;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'task_definition_type_id',
        'description',
        'category',
        'date_beg',
        'date_end',
        'plant_definition_id',
    ];
    /**
     * Attributes that have default values.
     */
    protected $attributes = [
        'task_definition_type_id' => TaskDefinitionType::DEFAULT_TYPE,
        'description' => 'toBeDefined',
    ];

    public function plant_definition(){
        return $this->belongsTo('App\Models\PlantDefinition', 'plant_definition_id');
    }
    public function task_definition_type(){
        return $this->belongsTo('App\Models\TaskDefinitionType', 'task_definition_type_idne_id');
    }
}